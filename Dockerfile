# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

COPY ./python3/requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
ENV PYTHONPATH="/app/python3"

COPY . .

CMD [ "python3", "./python3/services/acc_manager_service.py"]
