# -*- coding: utf-8 -*-
import sys

import requests
import tornado.wsgi
import tornado.httpserver
import tornado.ioloop
from logging.handlers import RotatingFileHandler
import errno
from global_def import *
import time
import os
import json
import logging
import subprocess

# region log
# ==============================================================================
def get_logger(name, level=logging.INFO, log_dir=None):
    try:
        # timecr = get_current_datetime()
        # timecr = timecr.strftime(DATETIME_FORMAT3).split("_")[0]
        logger = logging.getLogger(name)
        if log_dir is None:
            log_dir = LOG_DIR_ROOT
        try:
            # log_folder = log_dir + "/" + timecr
            os.makedirs(log_dir)
        except OSError as ex:
            if ex.errno != errno.EEXIST:
                logging.exception(ex)
            # else:
            #     try:
            #         os.makedirs(log_folder)
            #     except OSError as exx:
            #         if exx.errno != errno.EEXIST:
            #             logging.exception(exx)
        except Exception as ex:
            logging.exception(ex)

        # if level is not specified, use level of the root logger
        if level is None:
            level = logging.getLogger().getEffectiveLevel()

        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')

        file_h = RotatingFileHandler('{}/{}_{}.log'.format(log_dir, logging.getLevelName(level), name), mode='a',
                                     maxBytes=1024*1024*1024, backupCount=10, encoding=None, delay=0)
        file_h.setFormatter(formatter)

        stdout_h = logging.StreamHandler(sys.stdout)
        stdout_h.setFormatter(formatter)

        if (logger.hasHandlers()):
            logger.handlers.clear()

        logger.addHandler(file_h)
        logger.addHandler(stdout_h)
        logger.setLevel(level)

        return logger
    # ==============================================================================
    # endregion
    except Exception as e:
        logger_util.info("{}".format(e))

logger_util = get_logger("utils", level=logging.INFO)

def start_tornado(app, port):
    try:
        max_buffer_size = 1024 * 1024 * 20  # 20M
        http_server = tornado.httpserver.HTTPServer(
            tornado.wsgi.WSGIContainer(app),
            max_buffer_size=max_buffer_size,
        )

        http_server.listen(port)
        tornado.ioloop.IOLoop.instance().start()
    except Exception as e:
        logger_util.info("{}".format(e))

def stop_tornado():
    try:
        tornado.ioloop.IOLoop.instance().stop()
    except Exception as e:
        logger_util.info("{}".format(e))

def is_json(myjson):
    try:
        if isinstance(myjson, dict):
            return True
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True


def add_xmpp_user_ejabberd(username, caller = ""):
    data = {
        "user": username,
        "host": KAMAILIO_DOMAIN,
        "password": KAMAILIO_USER_PW
        }
    try:
        r = requests.post(XMPP_HTTP_SERVER + "/api/register", json=data)
        logger_util.info("SEND to {}/api/registered_users with status {}".format(XMPP_HTTP_SERVER, r.status_code))
        logger_util.info(r.content)

        if r.status_code == 200 or r.status_code == 409:
            return True
    except Exception as ex:
        logger_util.info("ERROR on creating XMPP user")

    return False

def send_xmpp_message(src = "", dest = "", message = ""):
    data = {
      "type": "chat",
      "from": src,
      "to": dest,
      "subject": "",
      "body": message
    }
    try:
        r = requests.post(XMPP_HTTP_SERVER + "/api/send_message", json=data)
        logger_util.info("SEND to {}/api/send_message with status {}".format(XMPP_HTTP_SERVER, r.status_code))
        logger_util.info(r.content)

        if r.status_code == 200:
            return True
    except Exception as ex:
        logger_util.info("ERROR on creating XMPP user")

    return False



def kamailio_user_convert(account):
    user = account.lower()
    if "@" in account:
        user = user.replace("@", "_")
    return user
