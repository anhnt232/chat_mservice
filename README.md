Welcome to the PNC Chat Server. This server require ejabberd, mysql, nginx and the python 3 web server Tornado. This server needs the following ports to be opened to the public world: 5443, 5222, 443.
 
        
## I. ejabberd Setup 

    Sửa file ejabberd.yml như sau: 
1. Sửa host với tên miền đang sử dụng, ở ví dụ dưới hãy sửa staging.wchat.vn thành tên miền của anh/chị

![Alt-Text](https://github.com/duyv2/readme/blob/main/host.png?raw=true)  

![Alt-Text](https://github.com/duyv2/readme/blob/main/host_1.png?raw=true)  

![Alt-Text](https://github.com/duyv2/readme/blob/main/host_2.png?raw=true)  

2. Tạo mysql database và bảng để ejabberd kết nối tới  
        - Tạo database ejabberd: CREATE DATABASE ejabberd;  
![Alt-Text](https://github.com/duyv2/readme/blob/main/create%20database%20ejabberd.png?raw=true)  
        - Sử dụng database vừa tạo: use ejabberd;  
![Alt-Text](https://github.com/duyv2/readme/blob/main/use%20ejabberd.png?raw=true)   
        - Tạo bảng từ script với đường dẫn tới file mysql.sql (script từ đường link  https://github.com/processone/ejabberd/blob/master/sql/mysql.sql  ): source /home/vuduy/mysql.sql;  
![Alt-Text](https://github.com/duyv2/readme/blob/main/create%20ejabberd%20table.png?raw=true)           
        - Tạo user: CREATE USER 'ejabberd_user'@'localhost' IDENTIFIED BY 'password';  
![Alt-Text](https://github.com/duyv2/readme/blob/main/create%20user%20for%20ejabberd.png?raw=true)         
        - Cấp quyền cho user trên sử dụng bảng : GRANT ALL PRIVILEGES ON *.* TO 'ejabberd_user'@'localhost';  
![Alt-Text](https://github.com/duyv2/readme/blob/main/grant%20all%20priv.png?raw=true)  

3. Nếu đã có mysql server ở bước 2, trong file ejabberd.yml thì tắt command của dòng code cấu hình kết nối đến mysql và điền thông tin vào từ dòng 33 đến dòng 41 theo thông tin của mysql anh đã cài đặt, dưới đây là ví dụ:    
![Alt-Text](https://github.com/duyv2/readme/blob/main/sql_1.png?raw=true)
![Alt-Text](https://github.com/duyv2/readme/blob/main/sql_2.png?raw=true)

4. Từ thư mục chứa file ejabberd.yml, chạy câu lệnh sau  
        docker run -d --name ejabberd -v $(pwd)/ejabberd.yml:/home/ejabberd/conf/ejabberd.yml -v $(pwd)/privkey.pem:/home/ejabberd/conf/privkey.pem -v $(pwd)/fullchain.pem:/home/ejabberd/conf/fullchain.pem -p 5222:5222 -p 5443:5443 -p 5281:5281 ejabberd/ecs

        Diễn giải
        --name: tên container
        -v: đưa file ejabberd.yml vào trong thư mục cấu hình của ejabberd 
        -p: mở port 5222 cho chat app, 5443 cho web, 5281 để mở api
        fullchain.pem, privkey.pem là 2 file cần có để kích hoạt ssl/tls  

        Hoặc chạy với câu lệnh  
        docker run -d --network=host --name ejabberd -v $(pwd)/ejabberd.yml:/home/ejabberd/conf/ejabberd.yml -v $(pwd)/privkey.pem:/home/ejabberd/conf/privkey.pem -v $(pwd)/fullchain.pem:/home/ejabberd/conf/fullchain.pem ejabberd/ecs  

        Diễn giải
        --name: tên container
        -v: đưa file ejabberd.yml vào trong thư mục cấu hình của ejabberd 
        --network=host : sử dụng chế độ network là host 
        fullchain.pem, privkey.pem là 2 file cần có để kích hoạt ssl/tls  

5. Để kiểm tra ejabberd đã chạy chưa, chạy câu lệnh sau  
        docker ps  
        Nếu chạy thành công sẽ hiển thị  
        ![Alt-Text](https://github.com/duyv2/readme/blob/main/dockerpssuccess.png?raw=true)  
        Nếu không thành công sẽ hiển thị  
        ![Alt-Text](https://github.com/duyv2/readme/blob/main/dockerpsfail.png?raw=true)  
        

## III. mservice  
       Đây là microservice quản lý account, source code của file nằm trong thư mục chat_mservice

1. Sửa lại cấu hình như sau  
        Sửa file global_def.py tại đường dẫn chat_mservice\python3\global_def.py  
        Sửa lại KAMAILIO_DOMAIN, đổi wchat.vn với domain đang sử dụng  
![Alt-Text](https://github.com/duyv2/readme/blob/main/mservicefix.png?raw=true)
        
2. cd vào thư mục chat_mservice, từ thư mục này build image với Dockerfile  
        Chạy câu lệnh   
        docker build --tag mservice .  

        Diễn giải  
        Docker tạo ra image từ code với tên là mservice  

        Quá trình chạy 
![Alt-Text](https://github.com/duyv2/readme/blob/main/createimage.png?raw=true)


3. Chạy container từ image vừa tạo  
        Chạy câu lệnh  
        docker run --name mservice -d --network=host mservice  

        Diễn giải  
        --name: Chạy container với tên là mservice  
        -d: chạy dưới background  
        --network=host : sử dụng chế độ network là host  

        Quá trình chạy  
![Alt-Text](https://github.com/duyv2/readme/blob/main/runiamge.png?raw=true)  
        
4. Kiểm tra nếu chạy thành công  
        Chạy câu lệnh  
        docker ps  
![Alt-Text](https://github.com/duyv2/readme/blob/main/psmservice.png?raw=true)  

        Chạy câu lệnh  
        docker logs mservice  


## IV. Cấu hình nginx

1. Cần forward https sang http về các api sau tại cổng 5001:  
        /api/v1/mobile/get_extension  
        /api/v1/get_extension   
        /api/v1/submit  

